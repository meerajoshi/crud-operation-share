package com.ignek.crudoperation.constants;

/**
 * @author ignek
 */
public class CrudOperationPortletKeys {

	public static final String CRUDOPERATION =
		"com_ignek_crudoperation_CrudOperationPortlet";

}