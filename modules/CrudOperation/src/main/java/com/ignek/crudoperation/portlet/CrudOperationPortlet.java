package com.ignek.crudoperation.portlet;

import com.ignek.crudoperation.constants.CrudOperationPortletKeys;
import com.ignek.student.model.Student;
import com.ignek.student.service.StudentLocalService;
import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

import java.util.List;

import java.io.IOException;

import javax.portlet.*;


import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author ignek
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=CrudOperation",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + CrudOperationPortletKeys.CRUDOPERATION,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class CrudOperationPortlet extends MVCPortlet {
private Log log = LogFactoryUtil.getLog(this.getClass().getName());
    
    @Reference
    CounterLocalService counterLocalService;
    
    @Reference
    StudentLocalService studentLocalService;
 
    @ProcessAction(name = "addStudent")
    public void addStudent(ActionRequest actionRequest,ActionResponse actionResponse) {
        long studentId = counterLocalService.increment(Student.class.getName());
        String enrollmentNo = ParamUtil.getString(actionRequest, "enrollmentNo");
        String fullName = ParamUtil.getString(actionRequest, "fullName");
        String RcontactNo = ParamUtil.getString(actionRequest, "contactNo");
        String email = ParamUtil.getString(actionRequest, "email");
        long contactNo=Long.parseLong(RcontactNo);
        
        log.info(studentId+enrollmentNo+fullName+contactNo+email);
    
        Student student = studentLocalService.createStudent(studentId);
        		//createStudent(studentId);
        student.setStudentId(studentId);
        student.setEnrollmentNo(enrollmentNo);
        student.setFullName(fullName);
        student.setContactNo(contactNo);
        student.setEmail(email);
        
        log.info(student);
        
        studentLocalService.addStudent(student);
        
        log.info("----------------------------Data Inserted Sucessfully !!---------------------------");
    }
    @Override
    public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException{
        List<Student> studentList = studentLocalService.getStudents(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
        renderRequest.setAttribute("studentList", studentList);        
        super.render(renderRequest, renderResponse);
    }
    
    @ProcessAction(name = "deleteStudent")
    public void deleteStudent(ActionRequest actionRequest, ActionResponse actionResponse){
        long studentId = ParamUtil.getLong(actionRequest, "studentId", GetterUtil.DEFAULT_LONG);
        try {
            studentLocalService.deleteStudent(studentId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    @ProcessAction(name = "updateStudent")
    public void updateStudent(ActionRequest actionRequest,  ActionResponse actionResponse) {
        long studentId = ParamUtil.getLong(actionRequest,"studentId", GetterUtil.DEFAULT_LONG);
        String enrollmentNo = ParamUtil.getString(actionRequest, "enrollmentNo", GetterUtil.DEFAULT_STRING);
        String fullName = ParamUtil.getString(actionRequest, "fullName", GetterUtil.DEFAULT_STRING);
        String RcontactNo = ParamUtil.getString(actionRequest, "contactNo", GetterUtil.DEFAULT_STRING);
        String email = ParamUtil.getString(actionRequest, "email", GetterUtil.DEFAULT_STRING);
        long contactNo=Long.parseLong(RcontactNo);
        
        Student student = null;
        try {
            student = studentLocalService.getStudent(studentId);
        } catch (Exception e) {
            log.error(e.getCause(), e);
        }
 
        if(Validator.isNotNull(student)) {
            student.setEnrollmentNo(enrollmentNo);
            student.setFullName(fullName);
            student.setContactNo(contactNo);
            student.setEmail(email);
            studentLocalService.updateStudent(student);
        }
    }
}