<%@ include file="init.jsp"%>
<portlet:defineObjects />
 
<portlet:actionURL name="addStudent" var="addStudentActionURL"/>
<portlet:renderURL var="goview">
    <portlet:param name="mvcPath" value="/view.jsp"/>
</portlet:renderURL>
<div class="mb-5">
    <a href="${goview}" class="btn  btn-primary btn-default">back</a>
</div>
                      
<h2>Add Student Form here !</h2>
 
<aui:form action="${addStudentActionURL}" name="studentForm" method="POST">
    <aui:input name="enrollmentNo" label="Enrollment No" type="text" >
         <aui:validator name="required"/>
         <aui:validator name="alphanum"/>
    </aui:input>
 
    <aui:input name="fullName" label="FullName" type="text">
         <aui:validator name="required"/>
         <aui:validator name="alpha"/>
    </aui:input>
 
    <aui:input name="contactNo" label="Contact no" type="text">
         <aui:validator name="required"/>
         <aui:validator name="digits"/>
         <aui:validator name="maxLength">10</aui:validator>
         <aui:validator name="minLength">10</aui:validator>
    </aui:input>
 
    <aui:input name="email" label="Email" type="text">
         <aui:validator name="required"/>
         <aui:validator name="email"/>
    </aui:input>  
 
    <aui:button type="submit" name="" value="Submit"></aui:button>
    <a href="${goview}" class="btn  btn-primary btn-default">Cancel</a>
</aui:form>
 