<%@ include file="init.jsp"%>
<portlet:defineObjects/>
<portlet:actionURL name="updateStudent" var="updateStudentActionURL"/>

<portlet:renderURL var="addStudentRenderURL">
    <portlet:param name="mvcPath" value="/view.jsp"/>
</portlet:renderURL>
<div class="mb-5">
    <a href="${addStudentRenderURL}" class="btn  btn-primary btn-default">back</a>
</div>
 <h2>Update Student Form here !</h2>
 <%
    String studentId = renderRequest.getParameter("studentId");
    String enrollmentNo = renderRequest.getParameter("enrollmentNo");
    String fullName = renderRequest.getParameter("fullName");
    String contactNo = renderRequest.getParameter("contactNo");
    String email = renderRequest.getParameter("email");
%>
<aui:form action="${updateStudentActionURL}" name="studentForm" method="POST">
	<aui:input name="studentId" type="hidden" value="<%=Long.parseLong(studentId)%>"></aui:input>
    <aui:input name="enrollmentNo" label="Enrollment No" type="text" value="${enrollmentNo}">
         <aui:validator name="required"/>
         <aui:validator name="alphanum"/>
    </aui:input>
 
    <aui:input name="fullName" label="FullName" type="text" value="${fullName}">
         <aui:validator name="required"/>
         <aui:validator name="alpha"/>
    </aui:input>
 
    <aui:input name="contactNo" label="Contact no" type="text" value="${contactNo}">
         <aui:validator name="required"/>
         <aui:validator name="digits"/>
    </aui:input>
 
    <aui:input name="email" label="Email" type="text" value="${email}">
         <aui:validator name="required"/>
         <aui:validator name="email"/>
    </aui:input>  
 
    <aui:button type="submit" name="" value="Update">
    </aui:button>
    <a href="${addStudentRenderURL}" class="btn  btn-primary btn-default">Cancel</a>
	
</aui:form>
